import * as React from 'react';
import './spoiler.css';
import block from 'bem-ts';
const b = block('spoiler');

interface IProps {
  label: string;
}

interface IState {
  isExpanded: boolean;
}

export class Spoiler extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = { isExpanded: false }
  }

  toggleExpand = () => {
    this.setState({ isExpanded: !this.state.isExpanded });
  };

  render() {
    return (
      <>
        <div className={b()} onClick={this.toggleExpand}>
          {this.props.label}
        </div>
        {
          <div className={b('content-wrapper')}>
            <div className={b('content', { show: this.state.isExpanded })}>
              {this.props.children}
            </div>
          </div>
        }
      </>
    );
  }
}