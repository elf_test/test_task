import * as React from 'react';
import './preloader.css';

export class Preloader extends React.Component {
  render() {
    return(
      <div className='preloader-wrapper'>
        <div className='preloader'/>
      </div>
    );
  }
}