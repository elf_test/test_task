import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPairs, fetchTradeHistory } from './store/actions/actions';
import {
  currencyPairsDataSelector, paginationDataSelector, preloaderStatusSelector, tradeHistorySelector
} from
  './store/selectors/selectors';
import { IPairData } from './store/states/pairsState';
import { Spoiler } from './components/spoiler';
import * as ReactPaginate from 'react-paginate';
import { IDataResponse } from './store/states/paginatedDataState';
import { Preloader } from './components/preloader';
import * as constants from './store/constants/constants';
import { TPreloaderState } from './store/states/preloaderState';
import './App.css';
import block from 'bem-ts';
const appBlock = block('app');
const headerBlock = block('header');
const bodyBlock = block('body');
const paginationBlock = block('pagination');

interface IProps {
  actions?: {
    fetchPairs: typeof fetchPairs;
    fetchTradeHistory: typeof fetchTradeHistory;
  }
  pairs?: IPairData;
  tradeHistory?: IDataResponse[];
  paginationData?: { perPage: number, total: number };
  showPreloader?: TPreloaderState;
}
interface IState {
  selectedPair: string;
  currentPage: number;
}

const mapStateToProps = (state) => ({
  pairs: currencyPairsDataSelector(state),
  tradeHistory: tradeHistorySelector(state),
  paginationData: paginationDataSelector(state),
  showPreloader: preloaderStatusSelector(state)
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({ fetchPairs, fetchTradeHistory }, dispatch)
});

class App extends React.Component<IProps, IState> {

  constructor(props) {
    super(props);
    this.state = { selectedPair: null, currentPage: 0 };
  }

  componentWillMount() {
    this.props.actions.fetchPairs();
  }

  handleChange = (key: string) => () => {
    this.setState({ selectedPair: key, currentPage: 0 });
    this.props.actions.fetchTradeHistory(key);
  };

  handlePageClick = (data) => {
    this.setState({ currentPage: data.selected });
  };

  render() {
    const { pairs, tradeHistory, paginationData, showPreloader } = this.props;
    const { selectedPair, currentPage } = this.state;
    const { perPage, total } = paginationData;
    const items = tradeHistory.slice(currentPage * perPage, currentPage * perPage + perPage);
    return (
      <div className={appBlock()}>
        {showPreloader === constants.LOADING ? <Preloader/> : null}
        <div className={headerBlock()}>
          <div className={headerBlock('nav')}>
            <Spoiler label='Show/hide pairs'>
              {Object.keys(pairs).map(key =>
                <div
                  key={pairs[key].id}
                  className={headerBlock('nav-link', { selected: selectedPair === key })}
                  onClick={this.handleChange(key)}
                >
                  {key}
                </div>
              )}
            </Spoiler>
          </div>
        </div>
        <div className={bodyBlock()}>
          <ReactPaginate
            pageCount={Math.ceil(total / perPage)}
            pageRangeDisplayed={5}
            marginPagesDisplayed={2}
            containerClassName={paginationBlock()}
            pageClassName={paginationBlock('link')}
            previousClassName={paginationBlock('link')}
            nextClassName={paginationBlock('link')}
            breakClassName={paginationBlock('link')}
            activeClassName={paginationBlock('link', { selected: true })}
            onPageChange={this.handlePageClick}
            forcePage={currentPage}
          />
          { items.map(i =>
            <div className={bodyBlock('entry')} key={i.globalTradeID}>
              {i.type + ' ' + i.amount + ' ' + i.total + ' ' + i.date}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
