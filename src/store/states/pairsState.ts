export const initialPairsState = {
  data: {},
  errorMessage: ''
};

export interface IPairData {
  id: number;
  last: string;
  lowestAsk: string;
  highestBid: string;
  percentChange: string;
  baseVolume: string;
  quoteVolume: string;
  isFrozen: string;
  high24hr: string;
  low24hr: string;
}

export interface IPairsResponse {
  [keys:string]: IPairData;
}

export interface IPairsState {
  data: IPairsResponse;
  errorMessage: string;
}