export const initialPaginatedDataState = {
  data: [],
  perPage: 50,
  total: 0,
  errorMessage: ''
}


export interface IDataResponse {
  globalTradeID: number;
  tradeID: number;
  date: string;
  type: 'buy' | 'sell';
  rate: number;
  amount: number;
  total: number;
}

export interface IPaginatedDataState {
  data: IDataResponse[],
  perPage: number,
  total: number,
  errorMessage: string
}