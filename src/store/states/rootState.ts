import { IPaginatedDataState } from './paginatedDataState';
import { IPairsState } from './pairsState';
import { TPreloaderState } from './preloaderState';

export interface IRootState {
  currencyPairs: IPairsState;
  paginatedData: IPaginatedDataState;
  status: TPreloaderState;
}