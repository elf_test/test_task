import { createSelector } from 'reselect';
import { IRootState } from '../states/rootState';
import { IPairsState } from '../states/pairsState';
import { IPaginatedDataState } from '../states/paginatedDataState';
import { TPreloaderState } from '../states/preloaderState';

const currencyPairsStateSlice = (state: IRootState): IPairsState => state.currencyPairs;

export const currencyPairsDataSelector = createSelector(
  currencyPairsStateSlice,
  (slice: IPairsState) => {
    return slice.data;
  }
);

const paginatedDataStateSlice = (state: IRootState): IPaginatedDataState => state.paginatedData;

export const tradeHistorySelector = createSelector(
  paginatedDataStateSlice,
  (slice: IPaginatedDataState) => {
    return slice.data;
  }
);

export const paginationDataSelector = createSelector(
  paginatedDataStateSlice,
  (slice: IPaginatedDataState) => {
    return { perPage: slice.perPage, total: slice.total };
  }
);

export const preloaderStatusSelector = (state: IRootState): TPreloaderState => state.status;