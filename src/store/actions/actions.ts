import * as constants from '../constants/constants';

export const fetchPairs = () => ({
  type: constants.FETCH_PAIRS_REQUEST,
  payload: null,
  meta: { status: constants.LOADING }
});

export const fetchPairsSuccess = (payload) => ({
  type: constants.FETCH_PAIRS_SUCCESS,
  payload,
  meta: { status: constants.LOADED }
});

export const fetchPairsFailure = (payload) => ({
  type: constants.FETCH_PAIRS_FAILURE,
  payload,
  meta: { status: constants.LOADED }
});

export const fetchTradeHistory = (payload) => ({
  type: constants.FETCH_TRADE_HISTORY_REQUEST,
  payload,
  meta: { status: constants.LOADING }
});

export const fetchTradeHistorySuccess = (payload) => ({
  type: constants.FETCH_TRADE_HISTORY_SUCCESS,
  payload,
  meta: { status: constants.LOADED }
});

export const fetchTradeHistoryFailure = (payload) => ({
  type: constants.FETCH_TRADE_HISTORY_FAILURE,
  payload,
  meta: { status: constants.LOADED }
});