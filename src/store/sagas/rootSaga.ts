import { fork } from 'redux-saga/effects';
import { fetchPairsWatcher } from './fetchPairsSaga';
import { fetchTradeHistoryWatcher } from './fetchTradeHistorySaga';

export function* rootSaga() {
  yield [
    fetchPairsWatcher,
    fetchTradeHistoryWatcher
  ].map(fork)
}