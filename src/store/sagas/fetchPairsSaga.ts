import { take, call, put } from 'redux-saga/effects';
import { fetchPairsSuccess, fetchPairsFailure } from '../actions/actions';
import * as constants from '../constants/constants';
import axios from 'axios';

function* fetchPairsSaga() {
  try {
    let result = yield axios.get('https://poloniex.com/public?command=returnTicker');
    yield put(fetchPairsSuccess(result.data));
  } catch (error) {
    yield put(fetchPairsFailure(error.message));
  }
}

export function* fetchPairsWatcher() {
  while (true) {
    yield take(constants.FETCH_PAIRS_REQUEST);
    yield call(fetchPairsSaga);
  }
}