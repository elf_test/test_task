import { take, call, put } from 'redux-saga/effects';
import { fetchTradeHistorySuccess, fetchTradeHistoryFailure } from '../actions/actions';
import * as constants from '../constants/constants';
import axios from 'axios';

function* fetchTradeHistorySaga(action) {
  try {
    let end = new Date();
    let start = new Date(end);
    start.setDate(start.getDate()-1);
    let startUNIX = Math.round(start.getTime() / 1000);
    let endUNIX = Math.round(end.getTime() / 1000);
    let result = yield axios.get('https://poloniex.com/public?command=returnTradeHistory&currencyPair=' +
      `${action.payload}&start=${startUNIX}&end=${endUNIX}`);
    yield put(fetchTradeHistorySuccess(result.data));
  } catch (error) {
    yield put(fetchTradeHistoryFailure(error.message));
  }
}

export function* fetchTradeHistoryWatcher() {
  while (true) {
    const action = yield take(constants.FETCH_TRADE_HISTORY_REQUEST);
    yield call(fetchTradeHistorySaga, action);
  }
}