import { combineReducers } from 'redux';
import { fetchReducer } from './fetchReducer';
import { paginatedDataReducer } from './paginatedDataReducer';
import { preloaderReducer } from './preloaderReducer';

export const rootReducer = combineReducers({
  currencyPairs: fetchReducer,
  paginatedData: paginatedDataReducer,
  status: preloaderReducer
});
