import { initialPairsState, IPairsState } from '../states/pairsState';
import * as constants from '../constants/constants';

export const fetchReducer = (state: IPairsState = initialPairsState, action) => {
  switch (action.type) {
    case constants.FETCH_PAIRS_SUCCESS:
      return { ...state, data: action.payload, errorMessage: '' };
    case constants.FETCH_PAIRS_FAILURE:
      return { ...state, data: {}, errorMessage: action.payload };
    default:
      return state;
  }
};
