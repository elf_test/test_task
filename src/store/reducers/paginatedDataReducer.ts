import { initialPaginatedDataState, IPaginatedDataState } from '../states/paginatedDataState';
import * as constants from '../constants/constants';

export const paginatedDataReducer = (state: IPaginatedDataState = initialPaginatedDataState, action) => {
  switch (action.type) {
    case constants.FETCH_TRADE_HISTORY_SUCCESS:
      return { ...state, data: action.payload, errorMessage:'', total: action.payload.length };
    case constants.FETCH_TRADE_HISTORY_FAILURE:
      return { ...state, data: {}, errorMessage: action.payload };
    default:
      return state;
  }
};
