import { TPreloaderState } from '../states/preloaderState';
import * as constants from '../constants/constants';

export const preloaderReducer = (state: TPreloaderState = constants.LOADED, action) => {
  switch (true) {
    default:
      // tslint:disable-next-line
      console.log(action);
      if (action.meta && action.meta.status) {
        return action.meta.status;
      }
      return state;
  }
};